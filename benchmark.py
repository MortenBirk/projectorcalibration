import json
from UnityHandler import UnityMapper
from LeastSquares import LeastSquare
import cv2
import numpy as np
import math


lsq = LeastSquare()
mapper = UnityMapper()

lsq.setResolution(1920, 1080)
mapper.setResolution(1920, 1080)
data = None;
data3D = None;
with open('2d32.txt', 'r') as infile:
    data = json.load(infile)
lsq.set2dPoints(data)
with open('3d32.txt', 'r') as infile:
    data3D = json.load(infile)
lsq.set3dPoints(data3D)
matrices = lsq.leastSquares()
res = json.loads(mapper.parse(matrices));

print("")
print("==============")
print("==============")
print("")
print("intrinsics")
print(json.dumps(res["intrinsics"], indent=4, sort_keys=True))
print("")
print("position")
print(json.dumps(res["position"], indent=4, sort_keys=True))
print("")
print("rotation")
print(json.dumps(res["angles"], indent=4, sort_keys=True))
print("")
print("Projection Frustum")
print(json.dumps(res["projectionMatrix"], indent=4, sort_keys=True))

print("")
print("==============")
print("==============")
print("")
print("rvec")
print(json.dumps(res["rotation"], indent=4, sort_keys=True))
print("tvec")
print(json.dumps(res["translation"], indent=4, sort_keys=True))
dist_coefs = np.zeros(4,'float32')


points, jacobian = cv2.projectPoints(lsq.getopencv3DPoints(), np.array(res["rotation"]), np.array(res["translation"]), np.array(res["intrinsics"]), dist_coefs)

errorX = 0
errorY = 0
for i in range(0, len(data)):
    ex = 0
    ey = 0
    ex = (data[i]["x"] - points[i][0][0])*(data[i]["x"] - points[i][0][0])
    ey = (data[i]["y"] - points[i][0][1])*(data[i]["y"] - points[i][0][1])
    print(ex)
    print(ey)
    errorX += ex
    errorY += ey
print("Mean squared error")
print("X")
print(errorX/len(data))
print("Y")
print(errorY/len(data))
