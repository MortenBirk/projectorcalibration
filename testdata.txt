/////////
IN Calibration
/////////
<projector_profile>
	<!-- Distance: 0.7135302988532557 -->
	<!-- Screen: 1080x1920 -->

	<adjustment_map>none.png</adjustment_map>
	<max_adjustment_offset>
		<x>0.019999999552965164</x>
		<y>0.019999999552965164</y>
	</max_adjustment_offset>
	<camera_fov>67.83193923593379</camera_fov>
	<camera_rotation_w>0.07853502787671734</camera_rotation_w>
	<camera_rotation_x>-0.7060046015988675</camera_rotation_x>
	<camera_rotation_y>-0.6943826266151185</camera_rotation_y>
	<camera_rotation_z>-0.11498921589747607</camera_rotation_z>
	<camera_position_x>-0.2822678201577627</camera_position_x>
	<camera_position_y>2.7518327688626965</camera_position_y>
	<camera_position_z>-1.6712556523947233</camera_position_z>
	<camera_aspect>0.5493265447608724</camera_aspect>
	<camera_lensshift_x>0.0</camera_lensshift_x>
	<camera_lensshift_y>-0.0036576381281922237</camera_lensshift_y>
	<camera_forward_x>-0.05329911323907441</camera_forward_x>
	<camera_forward_y>-0.27058520966992033</camera_forward_y>
	<camera_forward_z>0.9612194592473735</camera_forward_z>
	<camera_up_x>0.998536021873224</camera_up_x>
	<camera_up_y>-0.0233300345029841</camera_up_y>
	<camera_up_z>0.04880084539928921</camera_up_z>
</projector_profile>











<calibration_config>
	<screens>
		<screen id="0">
			<calibration_points>
				<point x="-0.4562" y="0.9944" z="0.1996" name="8" />
				<point x="-0.4402" y="1.1765" z="-0.0455" name="1" />
				<point x="-0.2165" y="0.9901" z="0.4549" name="6" />
				<point x="-0.2731" y="0.9986" z="-0.0611" name="2" />
				<point x="0.0198" y="1.1742" z="-0.0453" name="3" />
				<point x="-0.4289" y="1.1702" z="0.4435" name="7" />
				<point x="0.0334" y="0.9953" z="0.1869" name="4" />
				<point x="0.0225" y="1.1569" z="0.444" name="5" />
			</calibration_points>
			<user_points>
				<point x="0.2652777777777778" y="0.48463541666666665" id="-0.44021.1765-0.0455" />
				<point x="0.15787037037037038" y="0.546875" id="-0.27310.9986-0.0611" />
				<point x="0.2587962962962963" y="0.6575520833333333" id="0.01981.1742-0.0453" />
				<point x="0.28194444444444444" y="0.6479166666666667" id="0.03340.99530.1869" />
				<point x="0.4648148148148148" y="0.63515625" id="0.02251.15690.444" />
				<point x="0.3939814814814815" y="0.5622395833333333" id="-0.21650.99010.4549" />
				<point x="0.475" y="0.4989583333333333" id="-0.42891.17020.4435" />
				<point x="0.29212962962962963" y="0.4859375" id="-0.45620.99440.1996" />
			</user_points>
			<setting>
				<position x="-0.2822678201577627" y="2.7518327688626965" z="-1.6712556523947233" />
				<rotation x="265.0721074640016" y="271.94557524505495" z="74.24114977497064" />
				<aspect>0.5493265447608724</aspect>
				<fov>67.83193923593379</fov>

				<lensshiftx>0.0</lensshiftx>
				<lensshifty>-0.0036576381281922237</lensshifty>
				<distance>0.7135302988532557</distance>
			</setting>
			<last_saved_profile>C:\Users\UBI PC\Desktop\birkemil\CalibSoftware\1457180675022_screen1_profile.xml</last_saved_profile>
		</screen>
</calibration_config>











<projector_profile>
	<!-- Distance: 0.7135302988532557 -->
	<!-- Screen: 1080x1920 -->

	<adjustment_map>1457180675022_screen1_profile_adjustment_map.png</adjustment_map>
	<max_adjustment_offset>
		<x>0.019999999552965164</x>
		<y>0.019999999552965164</y>
	</max_adjustment_offset>
	<camera_fov>67.83193923593379</camera_fov>
	<camera_rotation_w>0.07853502787671734</camera_rotation_w>
	<camera_rotation_x>-0.7060046015988675</camera_rotation_x>
	<camera_rotation_y>-0.6943826266151185</camera_rotation_y>
	<camera_rotation_z>-0.11498921589747607</camera_rotation_z>
	<camera_position_x>-0.2822678201577627</camera_position_x>
	<camera_position_y>2.7518327688626965</camera_position_y>
	<camera_position_z>-1.6712556523947233</camera_position_z>
	<camera_aspect>0.5493265447608724</camera_aspect>
	<camera_lensshift_x>0.0</camera_lensshift_x>
	<camera_lensshift_y>-0.0036576381281922237</camera_lensshift_y>
	<camera_forward_x>-0.05329911323907441</camera_forward_x>
	<camera_forward_y>-0.27058520966992033</camera_forward_y>
	<camera_forward_z>0.9612194592473735</camera_forward_z>
	<camera_up_x>0.998536021873224</camera_up_x>
	<camera_up_y>-0.0233300345029841</camera_up_y>
	<camera_up_z>0.04880084539928921</camera_up_z>
</projector_profile>











<?xml version="1.0" encoding="UTF-8" ?>
<calibration_points>
	<scale>1.0</scale>
	<relative_point>
		<name>1</name>
		<x>-0.4402</x>
		<y>1.1765</y>
		<z>-0.0455</z>
	</relative_point>
	<relative_point>
		<name>2</name>
		<x>-0.2731</x>
		<y>0.9986</y>
		<z>-0.0611</z>
	</relative_point>
	<relative_point>
		<name>3</name>
		<x>0.0198</x>
		<y>1.1742</y>
		<z>-0.0453</z>
	</relative_point>
	<relative_point>
		<name>4</name>
		<x>0.0334</x>
		<y>0.9953</y>
		<z>0.1869</z>
	</relative_point>
	<relative_point>
		<name>5</name>
		<x>0.0225</x>
		<y>1.1569</y>
		<z>0.4440</z>
	</relative_point>
	<relative_point>
		<name>6</name>
		<x>-0.2165</x>
		<y>0.9901</y>
		<z>0.4549</z>
	</relative_point>
	<relative_point>
		<name>7</name>
		<x>-0.4289</x>
		<y>1.1702</y>
		<z>0.4435</z>
	</relative_point>
	<relative_point>
		<name>8</name>
		<x>-0.4562</x>
		<y>0.9944</y>
		<z>0.1996</z>
	</relative_point>
</calibration_points>










/////////
INSIDE UNITY
/////////

POSITION
-0.2822678
2.751833
1.671256

ROTATION
15.69909
183.1738
91.38864

PROJECTIONMATRIX
2.70743  0.00000  -0.99028  0.00000

0.00000  1.48726  0.00000   0.00000

0.00000  0.00000  -1.00020  -0.02000

0.00000	 0.00000  -1.00000  0.00000
