var canvas;
var ctx;
var currentPoint = 1;
var drawer;
//On load
$(() => {
  $(window).keypress((e) => {
    var code = e.keyCode || e.which;
      switch(code) {
        case 49:
          currentPoint = 1;
          break;
        case 50:
          currentPoint = 2;
          break;
        case 51:
          currentPoint = 3;
          break;
        case 52:
          currentPoint = 4;
          break;
        case 53:
          currentPoint = 5;
          break;
        case 54:
          currentPoint = 6;
          break;
        case 55:
          currentPoint = 7;
          break;
        case 56:
          currentPoint = 8;
          break;
        default:
          break;
      }
      $("#select-point-to-place")[0].value = currentPoint;
  });

  $("#fake-data-run").click((e) => {
    data = {};
    $.post("http://localhost/fake", JSON.stringify(data), (response) => {
      value = JSON.parse(response);
      console.log(value);
      for(var i = 0; i < 8; i++) {
        setNewPositionOfPoint({x:value.motive[i].x, y:value.motive[i].y, z:value.motive[i].z}, i);
      }
      $('#responseModal').modal('show');
      $('#modal-response-area').html("<div class='col-xs-4'><b>Intrinsics</b><br/>"
        + JSON.stringify(value.intrinsics) +
        "<br/></div> <div class='col-xs-4'><b>Rotation</b><br/>"
        + JSON.stringify(value.rotation) +
        "<br/></div> <div class='col-xs-4'><b>Translation</b><br/>"
        + JSON.stringify(value.translation) +
        "<br/></div> <div class='col-xs-12'><b>Position</b><br/>"
        + JSON.stringify(value.position) +
        "<br/></div> <div class='col-xs-12'><b>ProjectionMatrix</b><br/>"
        + formatJSON(JSON.stringify(value.projectionMatrix)) +
        "<br/></div> <div class='col-xs-12'><b>Rotation Euler</b><br/>"
        + JSON.stringify(value.angles) +
        "<br/></div>");
    })
  })

  $("#add-points-btn").click((e) => {
    var data = [];
    for(var i = 1; i < 9; i++) {
    point = drawer.getPoint(i);
    //Map points between 0 and 1, is this what i wan't?
    //point.x = point.x / binding.get("browserResX")
    //point.y = point.y / binding.get("browserResY")
      data.push(point);
    }
    console.log(data);
    $.post("http://localhost/points", JSON.stringify(data), (response) => {
      value = JSON.parse(response);

      for(var i = 0; i < 8; i++) {
        setNewPositionOfPoint({x:value.motive[i].x, y:value.motive[i].y, z:value.motive[i].z}, i);
      }
      $('#responseModal').modal('show');
    })
  });

  $("#calibrate-btn").click((e) => {
    $.post("http://localhost/calib", JSON.stringify([]), (response) => {
      value = JSON.parse(response);
      console.log(value);
      $('#responseModal').modal('show');
      $('#modal-response-area').html("<div class='col-xs-4'><b>Intrinsics</b><br/>"
        + JSON.stringify(value.intrinsics) +
        "<br/></div> <div class='col-xs-4'><b>Rotation</b><br/>"
        + JSON.stringify(value.rotation) +
        "<br/></div> <div class='col-xs-4'><b>Translation</b><br/>"
        + JSON.stringify(value.translation) +
        "<br/></div> <div class='col-xs-12'><b>Position</b><br/>"
        + JSON.stringify(value.position) +
        "<br/></div> <div class='col-xs-12'><b>ProjectionMatrix</b><br/>"
        + formatJSON(JSON.stringify(value.projectionMatrix)) +
        "<br/></div> <div class='col-xs-12'><b>Rotation Euler</b><br/>"
        + JSON.stringify(value.angles) +
        "<br/></div>");
    })
  });

  function formatJSON(str) {
    result = str.replace(/\[/g, "{");
    result = result.replace(/\]/g, "}");
    return result;
  }

  $(window).keydown((e) => {
    var code = e.keyCode || e.which;
    var xDir = 0;
    var yDir = 0;
    switch(code) {
      case 38:
        yDir = -1;
        break;
      case 40:
        yDir = 1;
        break;
      case 39:
        xDir = 1;
        break;
      case 37:
        xDir = -1;
        break;
      default:
        break;
    }
    drawer.movePoint(currentPoint, xDir, yDir);
    var point = drawer.getPoint(currentPoint);
    binding.set("point" + currentPoint + "X", point.x);
    binding.set("point" + currentPoint + "Y", point.y);
  });

  $("#select-points-button").click(() => {
    $("#main-menu-section").addClass("hide");
    $("#select-point-canvas-holder").removeClass("hide");
    canvas.width = binding.get("browserResX");
    canvas.height = binding.get("browserResY");
    drawer.init();
  });

  $("#hide-select-point-button").click(() => {
    $("#select-point-canvas-holder").addClass("hide");
    $("#main-menu-section").removeClass("hide");
  })

  $("#select-point-canvas").click((e) => {
    drawer.setPoint(currentPoint, e.pageX, e.pageY);
    var point = drawer.getPoint(currentPoint);
    binding.set("point" + currentPoint + "X", point.x);
    binding.set("point" + currentPoint + "Y", point.y);
  })

  $("#set-current-browser-resolution").click(() => {
    binding.set("browserResX", $(window).width());
    binding.set("browserResY", $(window).height());
    var sDataVal = {width: binding.get("browserResX"), height: binding.get("browserResY")};
    $.post("http://localhost/size", JSON.stringify(sDataVal), (response) => {
      console.log(JSON.parse(response));
    });
  })

  $("#set-kinect-resolution").click(() => {
    var sDataVal = {width: 512, height: 424};
    $.post("http://localhost/size", JSON.stringify(sDataVal), (response) => {
      console.log(JSON.parse(response));
    });
  })

  canvas = $("#select-point-canvas")[0];
  ctx = canvas.getContext("2d");
  drawer = new CanvasDrawer(ctx);
  document.getElementById("uploadimage").addEventListener("change", drawer.drawImageOnCanvas, false);

  var binding = new Binding("binding");
  binding.set("screenResX", 3200);
  binding.set("screenResY", 1600);
  binding.set("browserResX", 0);
  binding.set("browserResY", 0);
  for(var i = 1; i < 9; i++) {
    binding.set("point" + i + "X", 0);
    binding.set("point" + i + "Y", 0);
  }
});

var CanvasDrawer = (function() {
  var ctx = {};
  var points = [];
  var img = null;
  function CanvasDrawer(canvasContext) {
    ctx = canvasContext;
    for(var i = 1; i < 9; i++) {
      points.push({"id": i, "x": 0, "y": 0});
    }
  }

  CanvasDrawer.prototype.drawImageOnCanvas = function (ev) {
    console.log(ev);
    img = new Image(),
        f = document.getElementById("uploadimage").files[0],
        url = window.URL || window.webkitURL,
        src = url.createObjectURL(f);
    img.src = src;
    img.onload = function() {
        ctx.drawImage(img, 0, 0);
        url.revokeObjectURL(src);
    }
}

  CanvasDrawer.prototype.setPoint = function(number, xPos, yPos) {
    points.forEach((point) => {
      if(point.id === number) {
        point.x = xPos;
        point.y = yPos;
      }
    });
    redraw();
  };

  CanvasDrawer.prototype.movePoint = function(number, xDir, yDir) {
    points.forEach((point) => {
      if(point.id === number) {
        point.x += xDir;
        point.y += yDir;
      }
    });
    redraw();
  };

  CanvasDrawer.prototype.getPoint = function(number) {
    var res;
    points.forEach((point) => {
      if(point.id === number) {
        res = point;
      }
    });
    return res;
  };

  CanvasDrawer.prototype.init = function() {
    redraw();
  }

  function redraw() {
    console.log("removed");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if(img) {
      console.log("redraw");
      ctx.drawImage(img, 0, 0);
    }
    ctx.fillStyle="#FF0000";
    points.forEach((point) => {
      ctx.fillRect(point.x, point.y, 1, 1);
    });
  }
  return CanvasDrawer;
})();
