#REQUIREMENTS
#2 PLANES SHOULD BE SPLITTABLE BY A Y VALUE
#TOP SHOULD BE SPLITTABLE BY X AND Z VALUE AT center
#BOTTOM SHOULD HAVE 1 point being either heighes or lowest for each axis (biggest z, smallest z, biggest x, smallest x)

from random import shuffle


def sort(p, yHeight):
    top, bottom = sortOnY(p, yHeight)
    center = findCenter(top)
    top = sortTopPlane(top, center)
    center = findCenter(bottom)
    bottom = sortBottomPlane(bottom, center)
    return [top[0], bottom[0], top[1], bottom[1], top[2], bottom[2], top[3], bottom[3]]

def sortOnY(p, yHeight):
    top = []
    bottom = []
    for i in range(0,8):
        if p[i][1] > yHeight: #tHIS HAS TO BE MORE PRECISE
            top.append(p[i])
        else:
            bottom.append(p[i])
    return top, bottom


def findCenter(p):
    x = 0
    z = 0
    for i in range(0, len(p)):
        x += p[i][0]
        z += p[i][2]
    return [x / float(len(p)), z / float(len(p))]

def sortTopPlane(p, center):
    res = [None, None, None, None]
    #Going clockwise
    for i in range(0,len(p)):
        if p[i][0] < center[0] and p[i][2] < center[1]: #First quarter
            res[0] = p[i]
            continue
        if p[i][0] > center[0] and p[i][2] < center[1]: #Second quarter
            res[1] = p[i]
            continue
        if p[i][0] > center[0] and p[i][2] > center[1]: #Third quarter
            res[2] = p[i]
            continue
        if p[i][0] < center[0] and p[i][2] > center[1]: #Fourth quarter
            res[3] = p[i]
            continue

    return res

def sortBottomPlane(p, center):
    res = [None, None, None, None]
    #Going clockwise

    #First quarter
    best = None
    bestIndex = None
    for j in range(0,len(p)):
        if best == None:
            best = p[j]
            bestIndex = j
            continue
        if p[j][2] < best[2]:
            best = p[j]
            bestIndex = j
            continue
    res[0] = p[bestIndex]

    #Second quarter
    best = None
    bestIndex = None
    for j in range(0,len(p)):
        if best == None:
            best = p[j]
            bestIndex = j
            continue
        if p[j][0] > best[0]:
            best = p[j]
            bestIndex = j
            continue
    res[1] = p[bestIndex]

    #Third quarter
    best = None
    bestIndex = None
    for j in range(0,len(p)):
        if best == None:
            best = p[j]
            bestIndex = j
            continue
        if p[j][2] > best[2]:
            best = p[j]
            bestIndex = j
            continue
    res[2] = p[bestIndex]

    #Fourth quarter
    best = None
    bestIndex = None
    for j in range(0,len(p)):
        if best == None:
            best = p[j]
            bestIndex = j
            continue
        if p[j][0] < best[0]:
            best = p[j]
            bestIndex = j
            continue
    res[3] = p[bestIndex]
    return res




point1 = [0,1,0]
point2 = [0.5,0,0]
point3 = [1,1,0]
point4 = [1,0,0.5]
point5 = [1,1,1]
point6 = [0.5,0,1]
point7 = [0, 1, 1]
point8 = [0, 0, 0.5]


#points = [point1, point2, point3, point4, point5, point6, point7, point8]
#shuffle(points)
#res = sort(points)
#print(res)
